#
msgid ""
msgstr ""
"Project-Id-Version: documentation-develop-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-17 00:50+0000\n"
"PO-Revision-Date: 2023-01-18 13:29+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: HIG\n"

#: content/hig/_index.md:0
msgid "KDE Human Interface Guidelines"
msgstr "Guias de Interfaces Humanas do KDE"

#: content/hig/_index.md:9
msgid ""
"The **KDE Human Interface Guidelines (HIG)** offer designers and developers "
"a set of recommendations for producing beautiful, usable, and consistent "
"user interfaces for convergent desktop and mobile applications and workspace "
"widgets. Our aim is to improve the experience for users by making consistent "
"interfaces for desktop, mobile, and everything in between, more consistent, "
"intuitive and learnable."
msgstr ""
"As **Linhas-Guia de Interfaces Humanas do KDE (HIG)** oferecem aos "
"desenhadores e programadores um conjunto de recomendações para produzir "
"interfaces de utilizador bonitas, úteis e consistentes nas aplicações "
"convergentes para computadores e dispositivos móveis, bem como nos elementos "
"gráficos do ambiente de trabalho. O seu objectivo é melhorar a experiência "
"dos utilizadores, criando interfaces das aplicações e elementos mais "
"consistentes e, deste modo, mais intuitivas e fáceis de aprender."

#: content/hig/_index.md:15
msgid "Design Vision"
msgstr "Visão do Desenho"

#: content/hig/_index.md:18
msgid ""
"Our design vision focuses on two attributes of KDE software that connect its "
"future to its history:"
msgstr ""
"A visão do desenho geral foca-se em dois atributos das aplicações do KDE que "
"interligam o seu futuro com o seu histórico:"

#: content/hig/_index.md:21
msgid ""
"![Simple by default, powerful when needed.](/hig/HIGDesignVisionFullBleed."
"png)"
msgstr ""
"![Simples por omissão, poderoso quando necessário.](/hig/"
"HIGDesignVisionFullBleed.png)"

#: content/hig/_index.md:23
msgid "Simple by default..."
msgstr "Simples por omissão..."

#: content/hig/_index.md:25
msgid ""
"*Simple and inviting. KDE software is pleasant to experience and easy to use."
"*"
msgstr ""
"*Simples e convidativo. As aplicações do KDE são agradáveis de interagir e "
"fáceis de usar.*"

#: content/hig/_index.md:28
msgid "**Make it easy to focus on what matters**"
msgstr "**Facilitar o foco no que interessa**"

#: content/hig/_index.md:30
msgid ""
"Remove or minimize elements not crucial to the primary or main task. Use "
"spacing to keep things organized. Use color to draw attention. Reveal "
"additional information or optional functions only when needed."
msgstr ""
"Remover ou minimizar os elementos não cruciais para a tarefa principal. Use "
"espaços de intervalo para manter as coisas organizadas. Use cores para "
"chamar a atenção. Revele as informações adicionais ou funções opcionais só "
"quando for necessário."

#: content/hig/_index.md:35
msgid "**\"I know how to do that!\"**"
msgstr "**\"Sei como fazê-lo!\"**"

#: content/hig/_index.md:37
msgid ""
"Make things easier to learn by reusing design patterns from other "
"applications. Other applications that use good design are a precedent to "
"follow."
msgstr ""
"Torne as coisas mais simples de aprender, reutilizando padrões de desenho de "
"outras aplicações. As aplicações que usam um bom desenho são um precedente a "
"seguir."

#: content/hig/_index.md:41
msgid "**Do the heavy lifting for me**"
msgstr "**Fazer o trabalho pesado por mim**"

#: content/hig/_index.md:43
msgid ""
"Make complex tasks simple. Make novices feel like experts. Create ways in "
"which your users can naturally feel empowered by your software."
msgstr ""
"Torne simples as tarefas complexas. Faça com que os principiantes se sintam "
"especialistas. Crie formas nas quais os utilizadores se sintam naturalmente "
"reforçados pelo seu 'software'."

#: content/hig/_index.md:47
msgid "...Powerful when needed"
msgstr "...Poderoso quando necessário"

#: content/hig/_index.md:49
msgid ""
"*Power and flexibility. KDE software allows users to be effortlessly "
"creative and efficiently productive.*"
msgstr ""
"*Poder e flexibilidade. As aplicações do KDE permitem aos utilizadores serem "
"criativos sem esforço e produtivos de forma eficiente."

#: content/hig/_index.md:52
msgid "**Solve a problem**"
msgstr "**Resolver um problema**"

#: content/hig/_index.md:54
msgid ""
"Identify and make very clear to the user what need is addressed and how."
msgstr ""
"Identifique e torne bastante claro ao utilizador a necessidade que é tratada "
"e de que forma é feito."

#: content/hig/_index.md:57
msgid "**Always in control**"
msgstr "**Sempre no controlo**"

#: content/hig/_index.md:59
msgid ""
"It should always be clear what can be done, what is currently happening, and "
"what has just happened. The user should never feel at the mercy of the tool. "
"Give the user the final say."
msgstr ""
"Deverá ser sempre claro o que poderá ser feito, o que está a acontecer no "
"preciso momento e o que acabou de acontecer. O utilizador nunca se deve "
"sentir à mercê da ferramenta. Dê ao utilizador a última palavra."

#: content/hig/_index.md:64
msgid "**Be flexible**"
msgstr "**Ser flexível**"

#: content/hig/_index.md:66
msgid ""
"Provide sensible defaults but consider optional functionality and "
"customization options that don\\'t interfere with the primary task."
msgstr ""
"Forneça opções predefinidas válidas, mas tenha em consideração as "
"funcionalidades opcionais e as opções de personalização que não interfiram "
"com a tarefa principal."

#: content/hig/_index.md:70
msgid "Note"
msgstr "Nota"

#: content/hig/_index.md:72
msgid ""
"KDE encourages developing and designing for customization, while providing "
"good default settings. Integrating into other desktop environments is also a "
"virtue, but ultimately we aim for perfection within our own Plasma desktop "
"environment with the default themes and settings. This aim should not be "
"compromised."
msgstr ""
"O KDE encoraja o desenvolvimento e a concepção de personalizações, "
"fornecendo por outro lado boas opções predefinidas. A integração noutros "
"ambientes de trabalho também é uma virtude mas, em última instância, deve "
"atingir a perfeição dentro do nosso ambiente de trabalho Plasma com os temas "
"e opções predefinidas. Este objectivo não deverá ser comprometido."
